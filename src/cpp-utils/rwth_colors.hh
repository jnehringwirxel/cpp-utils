#pragma once

#include <typed-geometry/types/color.hh>

namespace util::rwth
{
// gamma corrected RWTH colors
// the gamma correction means that we can plug these directly into the glow-viewer without further adjustments
// based on https://www.rwth-aachen.de/global/show_document.asp?id=aaaaaaaabvgqfwx&download=1
constexpr tg::color3 blue = tg::color3(0, 0.0869013, 0.353741);
constexpr tg::color3 black = tg::color3(0, 0, 0);
constexpr tg::color3 magenta = tg::color3(0.774227, 0, 0.133209);
constexpr tg::color3 yellow = tg::color3(1, 0.851252, 0);
constexpr tg::color3 petrol = tg::color3(0, 0.119264, 0.130352);
constexpr tg::color3 turquoise = tg::color3(0, 0.320382, 0.363604);
constexpr tg::color3 green = tg::color3(0.0938759, 0.415148, 0.0160677);
constexpr tg::color3 may_green = tg::color3(0.517401, 0.618686, 0);
constexpr tg::color3 orange = tg::color3(0.923993, 0.399293, 0);
constexpr tg::color3 red = tg::color3(0.612066, 0.000367136, 0.00902149);
constexpr tg::color3 bordeaux = tg::color3(0.363604, 0.00226295, 0.0315514);
constexpr tg::color3 purple = tg::color3(0.119264, 0.0111261, 0.0962661);
constexpr tg::color3 lilac = tg::color3(0.197516, 0.160444, 0.420508);
constexpr tg::color3 white = tg::color3(1, 1, 1);

constexpr tg::color3 blue_100 = tg::color3(0, 0.0869013, 0.353741);
constexpr tg::color3 blue_75 = tg::color3(0.0477758, 0.215764, 0.481952);
constexpr tg::color3 blue_50 = tg::color3(0.275833, 0.499505, 0.789314);
constexpr tg::color3 blue_25 = tg::color3(0.579547, 0.729919, 0.891262);
constexpr tg::color3 blue_10 = tg::color3(0.812241, 0.88318, 0.95737);
constexpr tg::color3 black_100 = tg::color3(0, 0, 0);
constexpr tg::color3 black_75 = tg::color3(0.12753, 0.130352, 0.136099);
constexpr tg::color3 black_50 = tg::color3(0.339223, 0.348865, 0.353741);
constexpr tg::color3 black_25 = tg::color3(0.632043, 0.645555, 0.65237);
constexpr tg::color3 black_10 = tg::color3(0.84337, 0.851252, 0.851252);
constexpr tg::color3 magenta_100 = tg::color3(0.774227, 0, 0.133209);
constexpr tg::color3 magenta_75 = tg::color3(0.819964, 0.116576, 0.25084);
constexpr tg::color3 magenta_50 = tg::color3(0.88318, 0.348865, 0.447871);
constexpr tg::color3 magenta_25 = tg::color3(0.948965, 0.65237, 0.708298);
constexpr tg::color3 magenta_10 = tg::color3(0.982826, 0.859174, 0.875138);
constexpr tg::color3 yellow_100 = tg::color3(1, 0.851252, 0);
constexpr tg::color3 yellow_75 = tg::color3(1, 0.875138, 0.0891935);
constexpr tg::color3 yellow_50 = tg::color3(1, 0.91575, 0.334458);
constexpr tg::color3 yellow_25 = tg::color3(1, 0.95737, 0.645555);
constexpr tg::color3 yellow_10 = tg::color3(1, 0.982826, 0.859174);
constexpr tg::color3 petrol_100 = tg::color3(0, 0.119264, 0.130352);
constexpr tg::color3 petrol_75 = tg::color3(0.022013, 0.215764, 0.230998);
constexpr tg::color3 petrol_50 = tg::color3(0.20836, 0.378676, 0.394083);
constexpr tg::color3 petrol_25 = tg::color3(0.529523, 0.638779, 0.645555);
constexpr tg::color3 petrol_10 = tg::color3(0.796917, 0.84337, 0.84337);
constexpr tg::color3 turquoise_100 = tg::color3(0, 0.320382, 0.363604);
constexpr tg::color3 turquoise_75 = tg::color3(0, 0.447871, 0.481952);
constexpr tg::color3 turquoise_50 = tg::color3(0.254916, 0.612066, 0.632043);
constexpr tg::color3 turquoise_25 = tg::color3(0.598942, 0.804559, 0.804559);
constexpr tg::color3 turquoise_10 = tg::color3(0.835528, 0.923993, 0.923993);
constexpr tg::color3 green_100 = tg::color3(0.0938759, 0.415148, 0.0160677);
constexpr tg::color3 green_75 = tg::color3(0.271577, 0.535642, 0.116576);
constexpr tg::color3 green_50 = tg::color3(0.487765, 0.68002, 0.320382);
constexpr tg::color3 green_25 = tg::color3(0.729919, 0.835528, 0.625345);
constexpr tg::color3 green_10 = tg::color3(0.891262, 0.932277, 0.84337);
constexpr tg::color3 may_green_100 = tg::color3(0.517401, 0.618686, 0);
constexpr tg::color3 may_green_75 = tg::color3(0.638779, 0.701169, 0.106156);
constexpr tg::color3 may_green_50 = tg::color3(0.751895, 0.796917, 0.329729);
constexpr tg::color3 may_green_25 = tg::color3(0.875138, 0.899384, 0.638779);
constexpr tg::color3 may_green_10 = tg::color3(0.948965, 0.95737, 0.851252);
constexpr tg::color3 orange_100 = tg::color3(0.923993, 0.399293, 0);
constexpr tg::color3 orange_75 = tg::color3(0.95737, 0.523443, 0.0780566);
constexpr tg::color3 orange_50 = tg::color3(0.982826, 0.666117, 0.280124);
constexpr tg::color3 orange_25 = tg::color3(0.991393, 0.827726, 0.592438);
constexpr tg::color3 orange_10 = tg::color3(1, 0.932277, 0.827726);
constexpr tg::color3 red_100 = tg::color3(0.612066, 0.000367136, 0.00902149);
constexpr tg::color3 red_75 = tg::color3(0.694081, 0.106156, 0.0494335);
constexpr tg::color3 red_50 = tg::color3(0.796917, 0.311181, 0.193972);
constexpr tg::color3 red_25 = tg::color3(0.899384, 0.618686, 0.505432);
constexpr tg::color3 red_10 = tg::color3(0.95737, 0.835528, 0.774227);
constexpr tg::color3 bordeaux_100 = tg::color3(0.363604, 0.00226295, 0.0315514);
constexpr tg::color3 bordeaux_75 = tg::color3(0.476177, 0.0824142, 0.0915184);
constexpr tg::color3 bordeaux_50 = tg::color3(0.618686, 0.263175, 0.2468);
constexpr tg::color3 bordeaux_25 = tg::color3(0.789314, 0.56681, 0.535642);
constexpr tg::color3 bordeaux_10 = tg::color3(0.91575, 0.812241, 0.789314);
constexpr tg::color3 purple_100 = tg::color3(0.119264, 0.0111261, 0.0962661);
constexpr tg::color3 purple_75 = tg::color3(0.230998, 0.0738278, 0.180144);
constexpr tg::color3 purple_50 = tg::color3(0.399293, 0.238828, 0.348865);
constexpr tg::color3 purple_25 = tg::color3(0.65237, 0.535642, 0.618686);
constexpr tg::color3 purple_10 = tg::color3(0.851252, 0.789314, 0.827726);
constexpr tg::color3 lilac_100 = tg::color3(0.197516, 0.160444, 0.420508);
constexpr tg::color3 lilac_75 = tg::color3(0.334458, 0.288816, 0.541798);
constexpr tg::color3 lilac_50 = tg::color3(0.511398, 0.47044, 0.687031);
constexpr tg::color3 lilac_25 = tg::color3(0.737205, 0.708298, 0.835528);
constexpr tg::color3 lilac_10 = tg::color3(0.891262, 0.875138, 0.932277);
}
